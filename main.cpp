// #include "avl_tree/avl_tree.h"
// #include "sanity_test/avl_tree_orig.h"
#include "sanity_test/parity_check.h"
#include "test_vars.h"

#include <iostream>

using namespace std;


// this runs a test for insert & remove operation
// for both the standard & experimental trees
// then compare the results
int main () {
	avl_tree T1, *T2, *T1_orig, *T2_orig;
	parity_tree *pariT;
	bool status, status_orig;
	
	T1_orig = new avl_tree;
	printf("Inserting...\n");
	for (int i = 0; i < len_I; ++i) {
		avl_insert(&T1, I[i]);
		avl_insert_orig(T1_orig, I[i]);
	}
	printf("Count: %i\n", avl_count(&T1));
	pariT = parity_check(&T1, T1_orig, false, true);
	delete pariT;

	printf("Removing...\n");
	for (int i = 0; i < len_R; ++i) {
		avl_remove(&T1, R[i]);
		status_orig = avl_remove_orig(T1_orig, R[i]);
	}
	printf("Comparing T1 & T1_orig\n");
	pariT = parity_check(&T1, T1_orig, false, true);
	delete pariT;

	T2 = avl_copy(&T1);
	printf("Comparing T1 & T2\n");
	pariT = parity_check(&T1, T2, true, true);
	delete pariT;

	T2_orig = avl_copy_orig(T1_orig);
	printf("Comparing T1_orig & T2_orig\n");
	pariT = parity_check(T1_orig, T2_orig, false, true);
	delete pariT;

	// //compare also with T2

	delete T2;
	delete T1_orig;
	delete T2_orig;

	return 0;
}
