// A Simple AVL Tree Implementation
// by Timothy Kenno Handojo
/* Description: 
 * Barebone implementation of AVL Tree.
 * Meant to be employed into specialized uses when needed.
 */


// TODO: Deletion algorithm need work; incorect balance factor produced

#include <stdio.h>

#define _AVL_TREE 1


#ifndef KEYTYPE
	#define KEYTYPE unsigned int
#endif


struct avl_node {
	KEYTYPE id_num;
	avl_node *C[2];
	signed  balfac;
	
	avl_node(KEYTYPE N=0);
	virtual ~avl_node() {}
};

struct avl_tree {
	avl_node *root;

	avl_tree();
	~avl_tree();
};


avl_tree* avl_copy(avl_tree*);
avl_node* avl_fetch (avl_tree*, KEYTYPE);
void avl_insert (avl_tree*, KEYTYPE);
void avl_remove (avl_tree*, KEYTYPE);
unsigned avl_count (avl_tree*);
