// A Simple AVL Tree Implementation
// by Timothy Kenno Handojo
/* Description: 
 * Barebone implementation of AVL Tree.
 * Meant to be employed into specialized uses when needed.
 */


#include "avl_tree.h"


// BEGIN prototypes

void to_array(KEYTYPE*, unsigned&, avl_node*);

void destroy(avl_node*&);

unsigned count (avl_node*);

inline void rotate (avl_node*&, bool side);

avl_node *fetch (avl_node*,KEYTYPE);

int insert (avl_node*&,avl_node*,int);
inline void ins_case_ll (avl_node*&);
inline void ins_case_lr (avl_node*&);
inline void ins_case_rl (avl_node*&);
inline void ins_case_rr (avl_node*&);

int remove (avl_node*&,KEYTYPE I,int parent, avl_node*,bool,bool);
inline int rem_case_ll (avl_node*&);
inline int rem_case_lr (avl_node*&);
inline int rem_case_rl (avl_node*&);
inline int rem_case_rr (avl_node*&);

// END prototypes


// Node object constructor for convenience
avl_node::avl_node (KEYTYPE N): id_num(N), balfac(0) { C[0]=0; C[1]=0; }



avl_tree::avl_tree (): root(0) {} // Initialize root to NULL (for consistency)



avl_tree::~avl_tree () { destroy (root); } // Delete all the node--wrapper



// Recursive helper for destructor
void destroy (avl_node *& curr) {
	if (!curr) return;
	destroy (curr->C[0]);
	destroy (curr->C[1]);
	delete curr;
}

avl_tree* avl_copy (avl_tree *T) {
	unsigned c, i=0;
	KEYTYPE *temp;
	avl_tree *result;
	c = avl_count(T);
	temp = new KEYTYPE[c];
	result = new avl_tree;
	to_array(temp, i, T->root);
	for(i=0; i<c; ++i)
		avl_insert(result, temp[i]);
	return result;
}

void to_array(KEYTYPE *result, unsigned &i, avl_node *curr) {
	if(!curr) return;
	to_array(result, i, curr->C[0]);
	result[i] = curr->id_num;
	++i;
	to_array(result, i, curr->C[1]);
}

unsigned avl_count(avl_tree* T) { return count(T->root); }

unsigned count(avl_node* curr) {
	if (!curr) return 0;
	return 1 + count(curr->C[0]) + count(curr->C[1]);
}

// Return node with matching id_num--wrapper
avl_node* avl_fetch (avl_tree* T, KEYTYPE I) { return fetch (T->root, I); }

// Recursive helper for the function above
avl_node* fetch (avl_node *curr, KEYTYPE I) {
	if (!curr) return NULL;
	if (I == curr->id_num) return curr;
	if (I < curr->id_num) return fetch(curr->C[0], I);
	return fetch(curr->C[1], I);
}



inline void rotate (avl_node *&curr, bool side) {
	avl_node *temp = curr->C[!side];
	curr->C[!side] = temp->C[side];
	temp->C[side] = curr; curr = temp;
}



// Insertion method--wrapper
void avl_insert (avl_tree* T, KEYTYPE N) {
	avl_node * temp = new avl_node (N);
	insert (T->root, temp, 0);
}

// Recursive AVL tree insertion method
int insert (avl_node *&curr, avl_node *New, int parent) {
	int shift; // change in balfac (returned from recursive insert)
	int balfac; // computed balfac of current node after operation

	// Good ol' BST insertion, except for the return value
	if (!curr) { curr = New; return parent; }
	if (New->id_num < curr->id_num)
		shift = insert(curr->C[0],New,-1);
	else 
		shift = insert(curr->C[1],New, 1);	
	balfac = curr->balfac + shift;
	// The return value is used to compute balance factor

	// Decide if rotation is needed
	if (balfac > 1 || balfac < -1) {
		// Then decide which way they're leaning
		if (balfac < 0) {
			if (curr->C[0]->balfac < 0)
				ins_case_ll (curr);
			else ins_case_lr (curr);
		} else {
			if (curr->C[1]->balfac < 0)
				ins_case_rl (curr);
			else ins_case_rr (curr);
		} return 0;
	} else curr->balfac = balfac;
	// Otherwise save value to current node if no rotation happened

	if (shift && balfac) return parent; // Return leaning direction
	return 0; // Or nothing, if one of these happen:
	// - rotation has just been made (shift is 0)
	// - a subtree just balanced itself (balfac is 0)
	// This means that none of the ancestors' balfac will change
}

inline void ins_case_ll (avl_node *&curr) { // C on L, New on L:
	curr->balfac = 0; curr->C[0]->balfac = 0;
	rotate (curr, 1);
} // R-rot on curr

inline void ins_case_lr (avl_node *&curr) { // C on L, New on R:
	curr->balfac = 0; curr->C[0]->balfac = 0;
	rotate (curr->C[0], 0); rotate (curr, 1);
} // L-rot on C, R-rot on curr

inline void ins_case_rl (avl_node *&curr) { // C on R, New on L:
	curr->balfac = 0; curr->C[1]->balfac = 0;
	rotate (curr->C[1], 1); rotate (curr, 0);
} // R-rot on C, L-rot on curr

inline void ins_case_rr (avl_node *&curr) { // C on R, New on R:
	curr->balfac = 0; curr->C[1]->balfac = 0;
	rotate (curr, 0);
} // L-rot on curr



// Removal method--wrapper
void avl_remove (avl_tree* T, KEYTYPE I) {
    remove (T->root, I, 0, NULL, false, false);
}

// Recursive AVL tree removal method
#define REMOVAL 879345 // some arbitrary number to trigger node removal
int remove (avl_node *& curr, KEYTYPE I, int parent,
			avl_node* found, bool F, bool T) {
	int shift; // change in balfac (returned from recursive delete)
	avl_node *temp; // temporary variable for pointer exchange

	if (!curr) { // base cases
		if (!found) return 0; // match not found
		else return REMOVAL; // replacement found
		// delete replacement node on return
	}

	// check for what we're finding (match or replacement)
	if (!found) { // finding replacement if matching node is found
		if (I == curr->id_num) { // if match is found
			if (curr->C[1] || curr->C[0]) {
			// continue traversing for replacement
				T = (curr->C[1]); // deciding which way
				shift = remove(curr->C[T],I,2*T-1, curr,1,T);
			} else found = curr; // this means match is on leaf
		} else // now for the normal traversal in finding match
		if (I < curr->id_num)
			shift = remove (curr->C[0], I,-1, 0,0,0);
		else	shift = remove (curr->C[1], I, 1, 0,0,0);
	} // this allows fall-thru, which happens when match == replacement
	if (found) { // (i.e. match is found on leaf avl_node)
		// traverse til null
		if (F) T = !T; // change direction after first traversal
		// which is how inorder successor/predecessor is found
		shift = remove (curr->C[T],I,2*T-1, found,0,T);
		if (shift == REMOVAL) { // replacement found
			//replacing data to the matching node
			found->id_num = curr->id_num;
			temp = curr; curr = curr->C[!T];
			delete temp; return -1 * parent;
		} // then delete the replacement and return its position
	} curr->balfac += shift; // update balance factor

	switch (curr->balfac) { // decide on updating parent's balfac
	// deleted node is a child of 1-node parent:
	// parent gets balanced, grandparent gets shifted
	case (0): shift = -1 * parent; break;
	// deleted node is a child of 2-node parent:
	// parent gets shifted, nothing else
	case (1): case(-1): shift = 0; break;
	// rotation occurs: decide which way to rotate and shift
	case (-2): if (curr->C[0]->balfac > 0)
			shift = parent * rem_case_lr (curr);
		else	shift = parent * rem_case_ll (curr);
		break;
	case ( 2): if (curr->C[1]->balfac < 0)
			shift = parent * rem_case_rl (curr);
		else	shift = parent * rem_case_rr (curr);
		break;
	} // the rest is performed in rem_case_* functions

	return shift; // shift is now used to update parent balfac
}

inline int rem_case_ll (avl_node *&curr) { // C on L, New on L:
	int shift; // this will decide how curr's parent shifts
	if (curr->C[0]->balfac == 0) {
		curr->balfac = 1; curr->C[0]->balfac = 1;
		shift = 0; // height doesn't change for parent
	} else { // rotated nodes get balanced
		curr->balfac = 0; curr->C[0]->balfac = 0;
		shift = -1; // top node's parent gets shifted
	} rotate (curr, 1); return shift;
} // R-rot on curr

inline int rem_case_lr (avl_node *&curr) { // C on L, New on R:
	curr->C[0]->balfac = 0; rotate(curr->C[0],0);
	curr->balfac = 0; rotate(curr,1); // rotated nodes get balanced
	return -1; // top node's parent gets shifted
} // L-rot on C, R-rot on curr

inline int rem_case_rl (avl_node *&curr) { // C on R, New on L:
	curr->C[1]->balfac = 0; rotate(curr->C[1],1);
	curr->balfac = 0; rotate(curr,0); // rotated nodes get balanced
	return -1; // top node's parent gets shifted
} // R-rot on C, L-rot on curr

inline int rem_case_rr (avl_node *&curr) { // C on R, New on R:
	int shift = -1;
	if (curr->C[1]->balfac == 0) {
		curr->balfac = 1; curr->C[1]->balfac = 1;
		shift = 0; // height doesn't change for parent
	} else { // rotated nodes get balanced
		curr->balfac = 0; curr->C[1]->balfac = 0;
		shift = -1; // top node's parent gets shifted
	} rotate (curr, 0); return shift;
} // L-rot on curr
