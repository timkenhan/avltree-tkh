A Simple AVL Tree Implementation by Timothy Kenno Handojo

To modify the testcase, edit the variables:
- `I[]` for insertion data
- `R[]` for removal data

This contains experimental modification of balance factor shift algorithm.

The code is incomplete; see "avl_tree.h" for detail.
