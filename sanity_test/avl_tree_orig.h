// A Simple AVL Tree Implementation
// by Timothy K Handojo
/* Description:
 * This is a barebone implementation of Self-Balancing Binary Tree.
 * This base class is meant to be employed into specialized objects
 * appropriately when needed.
 */


#ifndef _AVL_TREE
    #include "../avl_tree/avl_tree.h"
#endif

#define _AVL_TREE_ORIG 1

#ifndef KEYTYPE
	#define KEYTYPE unsigned int
#endif


avl_tree* avl_copy_orig(avl_tree*);
// avl_node* avl_fetch (avl_tree*, KEYTYPE); // defined in avl_tree.h
void avl_insert_orig (avl_tree*, KEYTYPE);
bool avl_remove_orig (avl_tree*, KEYTYPE);
// unsigned avl_count (avl_tree*);  // defined in avl_tree.h
