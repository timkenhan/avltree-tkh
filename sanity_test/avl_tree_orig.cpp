// A Simple AVL Tree Implementation
// by Timothy K Handojo
/* Description:
 * This is a barebone implementation of Self-Balancing Binary Tree.
 * This base class is meant to be employed into specialized objects
 * appropriately when needed.
 */

#include "avl_tree_orig.h"
#include <cstdlib>
#include <algorithm>


using namespace std;


// BEGIN prototypes

void to_array(KEYTYPE*, unsigned&, avl_node*);

int height (const avl_node*curr);
inline void rotate (avl_node*&, bool side);

int avl_insert_orig (avl_node*&,avl_node*,int);
inline int ins_sort (avl_node*&, avl_node*,int);

int avl_remove_orig (avl_node*&,KEYTYPE,int,avl_node*,bool,bool);

inline void case_ll (avl_node*&);
inline void case_lr (avl_node*&);
inline void case_rl (avl_node*&);
inline void case_rr (avl_node*&);

// END prototypes



avl_tree* avl_copy_orig (avl_tree* T) {
	unsigned c, i=0;
	KEYTYPE *temp;
	avl_tree *result;
	c = avl_count(T);
	temp = new KEYTYPE[c];
	result = new avl_tree;
	to_array(temp, i, T->root);
	for(i=0; i<c; ++i)
		avl_insert_orig(result, temp[i]);
	return result;
}


int height (const avl_node *curr) {
	if (!curr) return 0;
	return 1 + max(height(curr->C[0]),height(curr->C[1]));
}


inline void rotate (avl_node *&curr, bool side) {
	avl_node *temp = curr->C[!side];
	curr->C[!side] = temp->C[side];
	temp->C[side] = curr;
	curr = temp;
}


// Generic BST insertion method--wrapper
void avl_insert_orig (avl_tree* T, KEYTYPE N) {
	avl_node *temp = new avl_node (N);
	avl_insert_orig (T->root,temp, 0);
}

// Generic BST insertion method--recursive helper
int avl_insert_orig (avl_node *&curr, avl_node *New, int parent) {
	int val, balfac, cbalfac;
	avl_node *child;

	// only insert at leaf; traversing to NULL means inserting
	if (!curr) { curr = New; return parent; }
	
	// traverse with the good ol' BST fashion
	if (New->id_num < curr->id_num)
		val = avl_insert_orig (curr->C[0],New,-1);
	else 
		val = avl_insert_orig (curr->C[1],New, 1);

	balfac = -height(curr->C[0]) +height(curr->C[1]);
	if (abs(balfac) > 1) { // decide if we need rotation
		if (balfac < 0) { // and if so, decide which way
			child = curr->C[0];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_ll (curr);
			else case_lr (curr);
		} else {
			child = curr->C[1];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_rl (curr);
			else case_rr (curr);
		}
	}
	return val;
}


// some arbitrary numbers
#define REMOVAL 879345 // node removal
#define NOTHERE 344112 // not found
#define SUCCESS 950608 // success

// Removal method--wrapper
bool avl_remove_orig (avl_tree* T, KEYTYPE I) {
	return (avl_remove_orig (T->root, I, 0, 0 ,0,0) == SUCCESS );
}

// Removal method--recursive helper
int avl_remove_orig (avl_node *&curr, KEYTYPE I, int parent,
			avl_node *found, bool F, bool T) {
	int val = 0;
       	int balfac, cbalfac;
	avl_node *temp, *child;

	if (!curr) { // base cases
		if (!found)
			return NOTHERE; // match not found
		else 	return REMOVAL; // replacement found
		// delete node replacement on return
	}

	// check for what we're finding (match or replacement)h
	if (!found) { // finding replacement if matching node is found
		if (I == curr->id_num) { // if match is found
			if (curr->C[1] || curr->C[0]) {
			// continue traversing for replacement
				T = (curr->C[1]); // direction of traversal
				val = avl_remove_orig (curr->C[T],I,parent, curr ,1,T);
			} else { // this means match is on leaf
				found = curr;
				val = REMOVAL;
			}
		} // now for the normal traversal in finding match
		else if (I < curr->id_num)
			val = avl_remove_orig (curr->C[0], I, -1, 0 ,0,0);
		else	val = avl_remove_orig (curr->C[1], I,  1, 0 ,0,0);
	} // this allows fall-thru, which happens when match == replacement
	if (found) { // (i.e. match is found on leaf node)
		// traverse til null
		if (F) T = !T;
		val = avl_remove_orig (curr->C[T],I,T,found ,0,T);
		if (val == REMOVAL) {
			// replacement found, replacing data to match
			// then delete replacement
			found->id_num = curr->id_num;
			temp = curr; curr = curr->C[!T];
			delete temp; return SUCCESS;
		}
	} else if (val == NOTHERE) return val;

	// compute balfac and decide if rotation is needed
	balfac = -height(curr->C[0]) +height(curr->C[1]);
	if (abs(balfac) > 1) {
		if (balfac < 0) {
			child = curr->C[0];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_ll (curr);
			else case_lr (curr);
		} else {
			child = curr->C[1];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_rl (curr);
			else case_rr (curr);
		}
	}
	return val;
}


// C on L, New on L: R-rot on curr
inline void case_ll (avl_node *&curr) { rotate (curr, 1); }

// C on L, New on R: L-rot on C, R-rot on curr
inline void case_lr (avl_node *&curr) { rotate(curr->C[0],0); rotate(curr,1); }

// C on R, New on L: R-rot on C, L-rot on curr
inline void case_rl (avl_node *&curr) { rotate(curr->C[1],1); rotate(curr,0); }

// C on R, New on R: L-rot on curr
inline void case_rr (avl_node *&curr) { rotate (curr, 0); }


