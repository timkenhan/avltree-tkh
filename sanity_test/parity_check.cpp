#include <cstdlib>
#include <cstring>
#include "parity_check.h"


void parity_check(avl_node*&, avl_node*&, parity_node*&, bool balfac, bool verbose);


char* to_char(void *num) {
    int *num_casted, num_str_len;
    char *num_str;
    num_casted = static_cast <int*> (num);
    if (num) {
        num_str_len = snprintf(NULL, 0, "%i", *num_casted);
        num_str = new char[num_str_len + 1];
        snprintf(num_str, num_str_len + 1, "%i", *num_casted);
    } else {
        num_str = new char[5];
        strcpy(num_str, "NULL");
    }
    return num_str;
}


parity_node::parity_node (avl_node *& actual, avl_node *& expected, bool balfac, bool verbose) {
    char *actual_id_str, *expected_id_str, *actual_bf_str, *expected_bf_str;

    actual_id = actual ? new KEYTYPE(actual->id_num) : NULL;
    expected_id = expected ? new KEYTYPE(expected->id_num) : NULL;
    if (balfac) {
        actual_bf = actual ? new int(actual->balfac) : NULL;
        expected_bf = expected ? new int(expected->balfac) : NULL;
    }
    parity = (
        ( (!actual_id && !expected_id) || !(!actual_id || !expected_id) && *actual_id==*expected_id ) &&
        ( !balfac || (!actual_bf && !expected_bf) || !(!actual_bf || !expected_bf) && *actual_bf==*expected_bf )
    );
    if (!parity && verbose) {
        printf("not match!\n");
        actual_id_str = to_char(actual_id);
        expected_id_str = to_char(expected_id);
        printf("expected_id: %s, actual_id: %s\n", expected_id_str, actual_id_str);
        delete actual_id_str;
        delete expected_id_str;
        if(balfac) {
            actual_bf_str = to_char(actual_bf);
            expected_bf_str = to_char(expected_bf);
            printf("expected_bf: %s, actual_bf: %s\n", expected_bf_str, actual_bf_str);
            delete actual_bf_str;
            delete expected_bf_str;
        }
        printf("\n");
    }
    C[0] = 0; C[1] = 0;
}

parity_tree::parity_tree (): root(0) {}

parity_tree::~parity_tree () { destroy (root); } // Delete all the node--wrapper

void parity_tree::destroy (parity_node *& curr) {
	if (!curr) return;
	destroy (curr->C[0]);
	destroy (curr->C[1]);
    if (curr->actual_id) delete curr->actual_id;
    if (curr->expected_id) delete curr->expected_id;
    if (curr->actual_bf) delete curr->actual_bf;
    if (curr->expected_bf) delete curr->expected_bf;
	delete curr;
}

parity_tree* parity_check(avl_tree *actual, avl_tree *expected, bool balfac, bool verbose){
    parity_tree *result = new parity_tree();
    parity_check(actual->root, expected->root, result->root, balfac, verbose);
    return result;
}

void parity_check(avl_node *& actual, avl_node *& expected, parity_node *& result, bool balfac, bool verbose){
    if (!actual && !expected) return;
    avl_node *actual_C0, *actual_C1, *expected_C0, *expected_C1;
    actual_C0 = actual ? actual->C[0] : NULL; expected_C0 = expected ? expected->C[0] : NULL;
    actual_C1 = actual ? actual->C[1] : NULL; expected_C1 = expected ? expected->C[1] : NULL;
    result = new parity_node(actual, expected, balfac, verbose);
    parity_check(actual_C0, expected_C0, result->C[0], balfac, verbose);
    parity_check(actual_C1, expected_C1, result->C[1], balfac, verbose);
}
