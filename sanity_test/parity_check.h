// requires avl_tree.h & avl_tree_orig.h

#ifndef _AVL_TREE_ORIG
    #include "avl_tree_orig.h"
#endif

#ifndef _AVL_TREE
    #include "../avl_tree/avl_tree.h"
#endif

#include <cstdio>


struct parity_node {
	KEYTYPE *actual_id;
    KEYTYPE *expected_id;
	int *actual_bf;
    int *expected_bf;
    bool parity;
	parity_node *C[2];
	
	parity_node(avl_node *& actual, avl_node *& expected, bool bf, bool verbose);
	virtual ~parity_node() {}
};

class parity_tree {
public:
	parity_node *root;
	parity_tree();
	~parity_tree();
private:
	void destroy(parity_node*&);
};

parity_tree* parity_check(avl_tree*, avl_tree*, bool bf=false, bool verbose=false);
